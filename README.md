# README

This package contains two Python modules:

  - sequence-parser
  - render-dialog

### sequence-parser

This is a standalone utility, that performs search in a given directory (and subdirectories) to find file sequences and prints out each sequence frame range (and gaps, if any found). By default it is searching for EXR files.

Example usage:
``` sh
$ python sequence-parser.py /path/to/directory
```

Optionally, you can specify more file extensions:

``` sh
$ python sequence-parser.py /path/to/directory -f exr,jpg,tiff
```

### render-dialog

This script is intended to be run in standalone mode.

Dependencies:

- Arnold Python API
- PySide

Example usage:
``` sh
$ python render-dialog.py
```
