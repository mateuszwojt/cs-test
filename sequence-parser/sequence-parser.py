import os
import sys
import fnmatch
import argparse

from operator import itemgetter
from itertools import groupby

def missingInSequence(*args):
    """
    Return gaps in a sequence of numbers (ints).
    """
    if len(args) == 1:
        start = args[0][0]
        end = args[0][-1]
    else:
        start = args[1]
        end = args[2]

    return sorted(set(range(start, end + 1)).difference(args[0]))

def sequenceParser(startDir, fileExtList=['exr']):
    """
    Print out all file sequences found in given directory (and sub-directories)
    with frame ranges and gaps (if found).

    :param startDir: directory in which the search is performed
    :param fileExtList: list of file extensions
    :return: None
    """
    for rootDir in os.walk(startDir):
        imageSeq = []

        for ext in fileExtList:
            matchFiles = sorted(fnmatch.filter(rootDir[2], '*.{}'.format(ext)))
            switchPad = 0

            if matchFiles:
                # find first and last frame of the sequence
                firstFrame = matchFiles[0].split('.')[-2]
                lastFrame = matchFiles[-1].split('.')[-2]

                for m in matchFiles:
                    seqName = '.'.join(m.split('.')[0:-2])

                    if seqName != '':
                        imageSeq.append([seqName, m])

                        if switchPad == 0:
                            # try to find padding in sequence
                            padding = len(list(m.split('.')[-2]))
                            switchPad = 1

                if imageSeq:
                    for key, group in groupby(imageSeq, itemgetter(0)):
                        # if padding not found, set it to 4 by default
                        if not padding:
                            padding = 4

                        serialNum = list()

                        # serialize frame numbers
                        for a in group:
                            serialNum.append(int(a[1].split('.')[-2]))

                        if serialNum:
                            # find gaps in frames sequence
                            missingFrames = missingInSequence(serialNum, int(firstFrame), int(lastFrame))

                            if missingFrames:
                                # group frame ranges, if gaps found
                                ranges = []
                                for k, g in groupby(enumerate(serialNum), lambda x: x[0] - x[1]):
                                    group = map(itemgetter(1), g)
                                    ranges.append("{:0{pad}d}-{:0{pad}d}".format(group[0], group[-1], pad=padding))

                                print('{}: {}'.format(key, ', '.join(ranges)))

                            else:
                                print('{}: {}-{}'.format(key, firstFrame, lastFrame))
            else:
                print("No matching file sequence found in: {}".format(rootDir[0]))
                print('-' * 64)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Sequence parser')
    parser.add_argument('directory', type=str, help="Specify directory where the search is performed", default=os.getcwd())
    parser.add_argument('-f', '--fileExt', nargs='+', help="Specify file extensions", default=['exr'])

    if len(sys.argv) < 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    sequenceParser(args.directory, args.fileExt)