# python
import os
import sys
import logging

from functools import partial

# PySide
try:
    from PySide import QtGui, QtCore
except ImportError as e:
    print("Cannot import PySide. {}".format(e))

# arnold
try:
    from arnold import *
except ImportError as e:
    print("Cannot import arnold API. {}".format(e))


logger = logging.getLogger(__name__)


class RenderLogger(logging.Handler):
    """
    Custom logging handler with QPlainTextEdit widget.
    """
    def __init__(self, parent):
        super(RenderLogger, self).__init__()
        self.widget = QtGui.QPlainTextEdit(parent)
        self.widget.setReadOnly(True)

    def emit(self, record):
        msg = self.format(record)
        self.widget.appendPlainText(msg)


class SceneRender(QtCore.QObject):
    """
    Prepares scene and runs Arnold render.
    """

    renderFinished = QtCore.Signal()

    def __init__(self, color, width, height, path):
        super(SceneRender, self).__init__()
        self.color = color
        self.width = width
        self.height = height
        self.path = path

    @QtCore.Slot()
    def run(self):
        # start Arnold session, log to file and the console
        AiBegin()
        AiMsgSetLogFileName(os.path.dirname(self.path) + os.path.sep + "scene.log")
        AiMsgSetConsoleFlags(AI_LOG_ALL)

        # create sphere
        sphere = AiNode("sphere")
        AiNodeSetStr(sphere, "name", "sphere")
        AiNodeSetVec(sphere, "center", 0, 4, 0)
        AiNodeSetFlt(sphere, "radius", 4)

        # create standard shader
        shader1 = AiNode("standard")
        AiNodeSetStr(shader1, "name", "sphereShd")

        # create tuple from QColor components
        sphereColor = (
            self.color.redF(),
            self.color.greenF(),
            self.color.blueF()
        )

        # set sphere diffuse color and spec size
        AiNodeSetRGB(shader1, "Kd_color", sphereColor[0], sphereColor[1], sphereColor[2])
        AiNodeSetFlt(shader1, "Ks", 0.05)

        # assign shader to geometry
        AiNodeSetPtr(sphere, "shader", shader1)

        # create perspective camera
        camera = AiNode("persp_camera")
        AiNodeSetStr(camera, "name", "mycamera")
        AiNodeSetVec(camera, "position", 0, 10, 35)
        AiNodeSetVec(camera, "look_at", 0, 3, 0)
        AiNodeSetFlt(camera, "fov", 45)

        # create point light source
        light1 = AiNode("point_light")
        AiNodeSetStr(light1, "name", "light1")
        AiNodeSetVec(light1, "position", 15, 30, 15)
        AiNodeSetFlt(light1, "intensity", 4500)
        AiNodeSetFlt(light1, "radius", 4)

        # create point light source
        light2 = AiNode("point_light")
        AiNodeSetStr(light2, "name", "light2")
        AiNodeSetVec(light2, "position", 15, 15, -30)
        AiNodeSetVec(light2, "color", 1.0, 0.2, 0.2)
        AiNodeSetFlt(light2, "intensity", 4500)
        AiNodeSetFlt(light2, "radius", 4)

        # create point light source
        light = AiNode("point_light")
        AiNodeSetStr(light, "name", "mylight")
        AiNodeSetVec(light, "position", -15, 30, -30)
        AiNodeSetFlt(light, "intensity", 4500)
        AiNodeSetFlt(light, "radius", 4)

        # global options
        options = AiUniverseGetOptions()
        AiNodeSetInt(options, "AA_samples", 8)
        AiNodeSetInt(options, "xres", self.width)
        AiNodeSetInt(options, "yres", self.height)
        AiNodeSetInt(options, "GI_diffuse_depth", 4)
        AiNodeSetPtr(options, "camera", camera)

        # create output driver
        driver = AiNode("driver_jpeg")
        AiNodeSetStr(driver, "name", "mydriver")
        AiNodeSetStr(driver, "filename", self.path)
        AiNodeSetFlt(driver, "gamma", 2.2)

        # create gaussian filter
        filter = AiNode("gaussian_filter")
        AiNodeSetStr(filter, "name", "myfilter")

        # assign driver and filter to main AOV
        outputs_array = AiArrayAllocate(1, 1, AI_TYPE_STRING)
        AiArraySetStr(outputs_array, 0, "RGBA RGBA myfilter mydriver")
        AiNodeSetArray(options, "outputs", outputs_array)

        # render image
        AiRender(AI_RENDER_MODE_CAMERA)

        # Arnold session shutdown
        AiEnd()

        # emit signal
        self.renderFinished.emit()


class RenderDialog(QtGui.QMainWindow, RenderLogger):
    """
    Main dialog.
    """
    def __init__(self):
        super(RenderDialog, self).__init__()

        # initialize basic values
        self._sphereColor = QtGui.QColor(255, 10, 10)
        self._logName = "scene.log"
        self._logPath = os.path.dirname(os.path.realpath(__file__)) + os.path.sep + self._logName
        self._fileName = "scene.jpg"
        self._filePath = os.path.dirname(os.path.realpath(__file__)) + os.path.sep + self._fileName
        self._imageHeight = 360
        self._imageWidth = 480

        # Set up main layout
        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.setContentsMargins(10, 10, 10, 10)
        self.mainLayout.setSpacing(2)
        self.mainWidget = QtGui.QWidget()

        # Rendered image group
        self.renderImageGrp = QtGui.QGroupBox("Rendered image")
        self.renderImageLyt = QtGui.QVBoxLayout()
        self.renderImageLbl = QtGui.QLabel()
        self.renderImageLyt.addWidget(self.renderImageLbl)
        self.renderImageGrp.setLayout(self.renderImageLyt)

        # Sphere color group
        self.colorPickerGrp = QtGui.QGroupBox("Sphere color")
        self.colorPickerLyt = QtGui.QHBoxLayout()
        self.colorPickerWgt = QtGui.QFrame()
        self.colorPickerWgt.setFixedSize(50, 50)
        self.colorPickerWgt.setObjectName("colorWidget")
        self.colorPickerBtn = QtGui.QPushButton("Pick color...")
        self.colorPickerLyt.addWidget(self.colorPickerWgt)
        self.colorPickerLyt.addWidget(self.colorPickerBtn)
        self.colorPickerGrp.setLayout(self.colorPickerLyt)

        # Render log group
        self.renderLogGrp = QtGui.QGroupBox("Render log")
        self.renderLogLyt = QtGui.QVBoxLayout()
        self.renderLogTextEdit = RenderLogger(self)
        self.renderLogLyt.addWidget(self.renderLogTextEdit.widget)
        self.renderLogGrp.setLayout(self.renderLogLyt)

        # Render button
        self.renderBtn = QtGui.QPushButton("Start render")

        # add all widgets to main layout
        self.mainLayout.addWidget(self.renderImageGrp)
        self.mainLayout.addWidget(self.colorPickerGrp)
        self.mainLayout.addWidget(self.renderLogGrp)
        self.mainLayout.addWidget(self.renderBtn)

        # connect layout to main widget
        self.mainWidget.setLayout(self.mainLayout)

        # set central widget
        self.setCentralWidget(self.mainWidget)

        # initialize UI and create connections
        self.initUI()
        self.createConnections()

        # setup logger
        logger.addHandler(self.renderLogTextEdit)
        logger.setLevel(logging.DEBUG)

    def initUI(self):
        # set window attributes, size and title
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setGeometry(100, 100, 750, 500)
        self.setWindowTitle("Render Dialog")

        # set QFrame background color (default: red)
        self.setWidgetBackgroundColor(self._sphereColor)

        # show window
        self.show()

    def createConnections(self):
        self.colorPickerBtn.clicked.connect(partial(self.openPickColorDialog))
        self.renderBtn.clicked.connect(partial(self.renderArnoldScene))

    def loadImage(self):
        try:
            print("Loading image: {}".format(self._filePath))

            if os.path.exists(self._filePath):
                # set image label pixmap to rendered image file
                pixmap = QtGui.QPixmap(self._filePath)
                self.renderImageLbl.setPixmap(pixmap)
                self.renderImageLbl.resize(self._imageWidth, self._imageHeight)

                return True
            else:
                raise Exception("File not found.")

        except Exception as e:
            print("Loading image failed: {}".format(e))

    def openPickColorDialog(self):
        # open color picker dialog
        dialog = QtGui.QColorDialog()
        ret = dialog.exec_()

        if ret == QtGui.QDialog.Accepted:
            # get selected color from dialog
            self._sphereColor = dialog.currentColor()

            # set QFrame background color to selected color
            self.setWidgetBackgroundColor(self._sphereColor)

            return

    def onRenderStart(self):
        # disable button during rendering
        self.renderBtn.setDisabled(True)
        # clear image label and set up some text
        self.renderImageLbl.clear()
        self.renderImageLbl.setFixedSize(self._imageWidth, self._imageHeight)
        self.renderImageLbl.setText("Scene is rendering...")

    def onRenderFinished(self):
        # enable button after render is finished
        self.renderBtn.setEnabled(True)
        self.renderImageLbl.clear()
        # load image from disk
        self.loadImage()
        # read log file
        self.readLog()

    def readLog(self):
        try:
            with open(self._logPath, 'r') as f:
                readLogData = f.read()
                logger.log(logging.INFO, readLogData)

            f.close()
        except Exception as e:
            print("Failed to read log: {}".format(e))

    def setWidgetBackgroundColor(self, color):
        p = self.colorPickerWgt.palette()
        p.setColor(self.colorPickerWgt.backgroundRole(), color)
        self.colorPickerWgt.setAutoFillBackground(True)
        self.colorPickerWgt.setPalette(p)

    def renderArnoldScene(self):
        self.onRenderStart()

        # run Arnold render in separate thread to avoid GUI hang-up
        self.thread = QtCore.QThread()
        self.sceneRender = SceneRender(
            color=self._sphereColor,
            width=self._imageWidth,
            height=self._imageHeight,
            path=self._filePath
        )
        self.sceneRender.moveToThread(self.thread)
        self.thread.started.connect(self.sceneRender.run)
        self.thread.start()
        self.sceneRender.renderFinished.connect(self.thread.terminate)
        self.sceneRender.renderFinished.connect(self.onRenderFinished)


def main():
    # create app instance
    app = QtGui.QApplication(sys.argv)
    mw = RenderDialog()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
